import csv
import os
import shlex
import subprocess
import sys

from subprocess import Popen, PIPE
from parsing_data import add_column_in_csv
from sys import stderr
import time

if __name__ == '__main__':

        time.sleep(3)
        #sfcapp_cmd_args = shlex.split("/home/hajar/ryu/bin/ryu-manager --verbose sfc_app.py")
        #sfcapp_process = Popen(sfcapp_cmd_args, stdout=PIPE, stderr=True, shell=True)
        sfcapp_cmd_args =  shlex.split("/home/hajar/ryu/bin/ryu-manager --verbose sfc_app.py")
        sfcapp_process = Popen(sfcapp_cmd_args, stdout=PIPE)

        #sfcapp_process = Popen("/home/hajar/ryu/bin/ryu-manager --verbose sfc_app.py", stdout=PIPE, stderr=True, shell=True)
        sfcapp_process.poll()
        time.sleep(60)

        #dataplane_cmd_args = shlex.split("./dataplane_setup.sh")
        #dataplane_process = Popen(dataplane_cmd_args, stdout=PIPE, stderr=None, shell=True)
        #dataplane_process = Popen("sudo ./dataplane_setup.sh", stdout=PIPE, stderr=None, shell=True)
        dataplane_cmd_args = shlex.split("./dataplane_setup.sh" )
        dataplane_process = Popen(dataplane_cmd_args, stdout=PIPE, stderr=None)

        dataplane_process.communicate()

        time.sleep(60)
#testing Reachability
        os.system("sudo ip netns exec client ping -c 10 10.0.0.5")

# running high number of simulations ( IPERF + Ping )
        for i in range(35):
                #server_cmd_args = shlex.split("sudo ip netns exec server python3 server.py")
                #server_process = Popen(server_cmd_args, stdout=PIPE, stderr=None, shell=True)
                #server_process = Popen("sudo ip netns exec server python3 server.py", stdout=PIPE, stderr=PIPE, shell=True)
                server_cmd_args = shlex.split("python3 server.py")
                server_process = Popen(server_cmd_args, stdout=PIPE, stderr=None)
                server_process.poll()
                time.sleep(5)

                #client_cmd_args = shlex.split("sudo ip netns exec server python3 client.py")
                #client_process = Popen(client_cmd_args, stderr=None, shell=True)
                #client_process = Popen("sudo ip netns exec server python3 client.py", stderr=None, shell=True)
                client_cmd_args = shlex.split("python3 client.py")
                client_process = Popen(client_cmd_args, stderr=None)
                client_process.communicate()
                time.sleep(5)
                client_process.kill()
                server_process.kill()
                os.remove("results/client.log")
# parsing results
                # BANDWIDTH
                #bandwidth_sim = open("results/bandwidth_sim.csv", "r", newline='')  # open input file for reading

                add_column_in_csv('results/bandwidth_sim.csv', 'results/bandwidth_all.csv',i)
                add_column_in_csv('results/transfer_sim.csv', 'results/transfer_all.csv', i)
                add_column_in_csv('results/rtt_sim.csv', 'results/rtt_all.csv', i)

        """     #TRANSFER

                #transfer_sim = open("results/transfer_sim.csv", "r")  # open input file for reading

                with open('results/transfer_all.csv', 'w',newline='') as f:  # output csv file
                        writer = csv.writer(f)
                        with open('results/transfer_sim.csv', 'r',newline='') as transfer_sim:  # input csv file
                                reader = csv.reader(transfer_sim, delimiter=',')
                                for row in reader:
                                        row[i] = transfer_sim.readline()  # edit the 8th column
                                        writer.writerow(row[i])
                transfer_sim.close()

        """
                # plot_cmd_args = shlex.split("sudo gnuplot gnuplot_script.p")
        #plot_process = Popen(plot_cmd_args, stdout=PIPE, stderr=stderr)
       # plot_process.poll()
        # Clean up:

        dataplane_process.kill()

        sfcapp_process.kill()


        #plot_process.terminate()



