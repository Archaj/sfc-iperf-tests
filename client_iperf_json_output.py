#!/usr/bin/python3.6
from subprocess import Popen

import iperf3
#the client instance
client = iperf3.Client()
#client.bind_address ='10.0.0.1'
client.server_hostname = '10.0.0.5'
client.port = 5565
client.protocol = 'udp'
client.verbose = True
client.reverse = True
client.bandwidth = 8589934592 # with bits/second == 1G/s
client.duration = 10 # 10seconds
 # still need to find how to set the number of bytes to transmit
#client.json_output = True
result = client.run()
print(result)
f= open("iperf_client_result.txt","w+")
result=str(result)
f.write(result)
f.close()
    #print('Connecting to {0}:{1}'.format(client.server_hostname, client.port))
#result = client.run() # normally this should print in the screen

"""if result.error:
    print(result.error)
else:
    print('')
    print('Test completed:')
    print('  started at         {0}'.format(result.time))
    print('  bytes transmitted  {0}'.format(result.bytes))
    print('  jitter (ms)        {0}'.format(result.jitter_ms))
    print('  avg cpu load       {0}%\n'.format(result.local_cpu_total))

    print('Average transmitted data :')
    print('  Kilobits per second  (kbps)  {0}'.format(result.kbps))
"""

