import os.path as path

import pandas as pd
def add_column_in_csv(input_file, output_file,position):
    input = pd.read_csv(input_file, header=None, escapechar=';')
    if path.exists(output_file):
        output = pd.read_csv(output_file, header=None)
    else:
        output=pd.DataFrame()

    #to delete the last line which is a summ or mean
    #input[0].drop(input[0].tail(1).index, inplace=True )
    output[position] = input[0]
    output.to_csv(output_file, index=False, header=False)
    input.to_csv(input_file)
    
    #data = pd.read_csv (input_file)
    #data.to_csv(output_file,mode='a',index=False)
""""
    dict = pd.read_csv(input_file)
    df= pd.DataFrame(dict)
    list= df.values.tolist()
    data = pd.read_csv(output_file)
    #data.insert(position,list)
    #col_name = str(position)
    data.append(df)
    data.append("____")
    data.to_csv(output_file,index=False)
"""
"""
import csv
from csv import writer
from csv import reader


    #Append a column in existing csv using csv.reader / csv.writer classes
    with open(input_file, 'r') as sim:
        with open(output_file, 'r+') as all:
            reader1 = csv.reader(sim, delimiter=',')
            reader2 = csv.reader(all, delimiter=',')

            both = []
            #fields = reader1.next()  # read header row
            #reader2.next()  # read and ignore header row
            line=1
            for row1, row2 in zip(reader1, reader2):
                print(line)
                row2.append(row1[line])
                both.append(row2)
                line = line + 1
            with open(output_file, 'w') as output:
                writer = csv.writer(output, delimiter=',')
                #writer.writerow(fields)  # write a header row
                writer.writerows(both)
                output.close()
        all.close()
    sim.close()
"""    
"""
    # Open the input_file in read mode and output_file in write mode
    with open(input_file, 'r') as read_obj, \
            open(output_file, 'w+', newline='') as write_obj:
        # Create a csv.reader object from the input file object
        csv_reader = reader(read_obj)
        # Create a csv.writer object from the output file object
        csv_writer = writer(write_obj)
        # Read each row of the input csv file as list
        line=1
        for row in csv_reader:
            # Pass the list / row in the transform function to add column text for this row

            row1 = write_obj.readline(line)
            print(row1)
            row1.append(row[0])
            #write_obj.write(row1)
           # transform_row(row, csv_reader.line_num)
            # Write the updated row / list to the output file
            csv_writer.writerow(row1)
            line=line+1
"""