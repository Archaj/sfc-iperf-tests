import shlex
from subprocess import Popen, PIPE
from sys import stderr
import os
import csv
import time
#running iperf client
client_cmd_args= shlex.split("ip netns exec client iperf3 -c 10.0.0.5 -u -p 5565 -n 1000000000 -l 1400 -b 1G -R -i 1 --logfile results/client.log")
#client_cmd_args= shlex.split("ip netns exec client iperf3 -c 10.0.0.5 -u -p 5565 -n 1000000000 -l 1400 -b 1G -R -i 1 > results/client.log")
print(client_cmd_args)
iperf_client_process = Popen(client_cmd_args, stdout=PIPE, stderr=stderr)
result=iperf_client_process.communicate()
print(result)
rcode= iperf_client_process.returncode
print("Return code:")
print(rcode)
print("-----------------------------")
iperf_client_process.terminate()
iperf_client_process.kill()

## stripping the result file to get ready to plot files
#os.system("tmp=$(cat -n results/client.log | awk '/sec/ {print $6}')")
#os.system("awk -F, '{$(NF+1)=tmp;}1' OFS=, >>results/transfer.csv")# in MBytes
os.system("awk '/sec/ {print $5}' results/client.log > results/transfer_sim.csv")
#os.system("awk '{$3=$1;print}' results/tmp.csv >> results/transfer0.csv")
os.system("awk '/sec/ {print $7}' results/client.log > results/bandwidth_sim.csv") #in Mbits/sec

#os.system("cat results/client.dat | awk '/sec/ {print $7}' > results/bandwidth_sim.csv") #in Mbits/sec
#os.system("awk '{$3=$1;print}' results/tmp.csv >> results/bandwidth0.csv")
#ping to get RTT
os.system("ip netns exec client ping -c 100 10.0.0.5 > results/ping.csv")# modified > instead of >>
os.system("awk '/time=/ {print $7}' results/ping.csv > results/rtt_sim.csv")
os.system("sed -i 's/time=//' results/rtt_sim.csv")
os.system("sed -i 's/icmp_seq=//' results/rtt_sim.csv")
#I still need to specify the OFS for each file before

#os.system("awk -F, '{$1=++i FS $1;}1' OFS=, results/transfer0.csv > results/transfer.csv")
#os.system("awk -F, '{$1=++i FS $1;}1' OFS=, results/bandwidth0.csv > results/bandwidth.csv")

