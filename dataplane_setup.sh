#!/bin/bash

# Cleaning
#
# bridges including incgree and egress classifiers and SFFs

# or simlpy:

for bridge in `ovs-vsctl list-br`; do
               sudo ovs-vsctl del-br $bridge
               done

#sudo ovs-vsctl --if-exists del-br cl_in
#sudo ovs-vsctl --if-exists del-br sff1
#sudo ovs-vsctl --if-exists del-br sff2
#sudo ovs-vsctl --if-exists del-br sff3
#sudo ovs-vsctl --if-exists del-br sff4
#sudo ovs-vsctl --if-exists del-br cl_eg

sudo link delete veth_server_int
sudo link delete veth_client_int

# interfaces of SFs, each SF have two interfaces, one is connected to one sff and the other interface is connected to the next sff. the interface number represents the SF number _ the sff number
sudo ip link delete veth_sf2_int2_1
sudo ip link delete veth_sf2_int2_2
sudo ip link delete veth_sf1_int1_2
sudo ip link delete veth_sf1_int1_3
sudo ip link delete veth_sf3_int3_3
sudo ip link delete veth_sf3_int3_4

sudo ip link delete veth_sf2_ext2_1
sudo ip link delete veth_sf2_ext2_2
sudo ip link delete veth_sf1_ext1_2
sudo ip link delete veth_sf1_ext1_3
sudo ip link delete veth_sf3_ext3_3
sudo ip link delete veth_sf3_ext3_4


sudo ip link delete veth_server_ext
sudo ip link delete veth_client_ext

#link between Cl_in and SFF1
sudo ip link delete link0_1
sudo ip link delete link1_0

sudo ip link delete link1_2
sudo ip link delete link2_1

sudo ip link delete link2_3
sudo ip link delete link3_2

sudo ip link delete link3_4
sudo ip link delete link4_3
#link between SFF4 and CL_eg
sudo ip link delete link4_5
sudo ip link delete link5_4

#sudo ip - all netns delete #No netns name specified
sudo ip netns delete server #client sf1 sf2 sf3
sudo ip netns delete client
sudo ip netns delete sf2
sudo ip netns delete sf1
sudo ip netns delete sf3

echo "Env cleared"


sudo ip netns add client
sudo ip netns add server
#the list of SFs
sudo ip netns add sf2
sudo ip netns add sf1
sudo ip netns add sf3


sudo ip link add veth_server_int type veth peer name veth_server_ext
sudo ip link add veth_client_int type veth peer name veth_client_ext

#defining links for SFs, the order of the links is very important it defines the ports numbers

sudo ip link add veth_sf2_int2_1 type veth peer name veth_sf2_ext2_1
sudo ip link add veth_sf1_int1_2 type veth peer name veth_sf1_ext1_2
sudo ip link add veth_sf3_int3_3 type veth peer name veth_sf3_ext3_3
sudo ip link add veth_sf2_int2_2 type veth peer name veth_sf2_ext2_2
sudo ip link add veth_sf1_int1_3 type veth peer name veth_sf1_ext1_3
sudo ip link add veth_sf3_int3_4 type veth peer name veth_sf3_ext3_4


sudo ip link set veth_server_int netns server
sudo ip link set veth_client_int netns client
#setting links for SFs

sudo ip link set veth_sf2_int2_1 netns sf2
sudo ip link set veth_sf2_int2_2 netns sf2
sudo ip link set veth_sf1_int1_2 netns sf1
sudo ip link set veth_sf1_int1_3 netns sf1
sudo ip link set veth_sf3_int3_3 netns sf3
sudo ip link set veth_sf3_int3_4 netns sf3



#temp comment: ip addresses modified
sudo ip netns exec server ip addr add 10.0.0.5/24 dev veth_server_int
sudo ip netns exec client ip addr add 10.0.0.1/24 dev veth_client_int
# ip addresses of SFs
sudo ip netns exec sf2 ip addr add 10.0.0.3/24 dev veth_sf2_int2_1
sudo ip netns exec sf1 ip addr add 10.0.0.2/24 dev veth_sf1_int1_2
sudo ip netns exec sf3 ip addr add 10.0.0.4/24 dev veth_sf3_int3_3
sudo ip netns exec sf2 ip addr add 10.0.0.13/24 dev veth_sf2_int2_2
sudo ip netns exec sf1 ip addr add 10.0.0.12/24 dev veth_sf1_int1_3
sudo ip netns exec sf3 ip addr add 10.0.0.14/24 dev veth_sf3_int3_4

#precising the Mac addresses

sudo ip netns exec client ip link set dev veth_client_int address 00:00:00:00:00:01
sudo ip netns exec server ip link set dev veth_server_int address 00:00:00:00:00:05
sudo ip netns exec sf2 ip link set dev veth_sf2_int2_1 address 00:00:00:00:00:03
sudo ip netns exec sf1 ip link set dev veth_sf1_int1_2 address 00:00:00:00:00:02
sudo ip netns exec sf3 ip link set dev veth_sf3_int3_3 address 00:00:00:00:00:04
sudo ip netns exec sf2 ip link set dev veth_sf2_int2_2 address 00:00:00:00:00:13
sudo ip netns exec sf1 ip link set dev veth_sf1_int1_3 address 00:00:00:00:00:12
sudo ip netns exec sf3 ip link set dev veth_sf3_int3_4 address 00:00:00:00:00:14

sudo ovs-vsctl add-br cl_in
sudo ovs-vsctl add-br sff1
sudo ovs-vsctl add-br sff2
sudo ovs-vsctl add-br sff3
sudo ovs-vsctl add-br sff4
sudo ovs-vsctl add-br cl_eg

ovs-vsctl set bridge cl_in other-config:datapath-id=0000000000000001
ovs-vsctl set bridge sff1 other-config:datapath-id=0000000000000002
ovs-vsctl set bridge sff2 other-config:datapath-id=0000000000000003
ovs-vsctl set bridge sff3 other-config:datapath-id=0000000000000004
ovs-vsctl set bridge sff4 other-config:datapath-id=0000000000000005
ovs-vsctl set bridge cl_eg other-config:datapath-id=0000000000000006
#connect the switches to the controller
sudo ovs-vsctl set-controller cl_in tcp:127.0.0.1:6633
sudo ovs-vsctl set-controller sff1 tcp:127.0.0.1:6633
sudo ovs-vsctl set-controller sff2 tcp:127.0.0.1:6633
sudo ovs-vsctl set-controller sff3 tcp:127.0.0.1:6633
sudo ovs-vsctl set-controller sff4 tcp:127.0.0.1:6633
sudo ovs-vsctl set-controller cl_eg tcp:127.0.0.1:6633


sudo ip addr add 10.0.0.253/24 dev cl_in
sudo ip link set cl_in up
sudo ip addr add 10.0.0.254/24 dev cl_eg
sudo ip link set cl_in up

sudo ovs-vsctl add-port cl_in veth_client_ext
sudo ovs-vsctl add-port cl_eg veth_server_ext
sudo ovs-vsctl add-port sff1 veth_sf2_ext2_1
sudo ovs-vsctl add-port sff2 veth_sf1_ext1_2
sudo ovs-vsctl add-port sff2 veth_sf2_ext2_2
sudo ovs-vsctl add-port sff3 veth_sf3_ext3_3
sudo ovs-vsctl add-port sff3 veth_sf1_ext1_3
sudo ovs-vsctl add-port sff4 veth_sf3_ext3_4

sudo ip netns exec client ip link set veth_client_int up
sudo ip netns exec server ip link set veth_server_int up
sudo ip netns exec sf2 ip link set  veth_sf2_int2_1 up
sudo ip netns exec sf2 ip link set  veth_sf2_int2_2 up
sudo ip netns exec sf1 ip link set  veth_sf1_int1_2 up
sudo ip netns exec sf1 ip link set  veth_sf1_int1_3 up
sudo ip netns exec sf3 ip link set  veth_sf3_int3_3 up
sudo ip netns exec sf3 ip link set  veth_sf3_int3_4 up



sudo ip link set veth_server_ext up
sudo ip link set veth_client_ext up
sudo ip link set veth_sf2_ext2_1 up
sudo ip link set veth_sf2_ext2_2 up
sudo ip link set veth_sf1_ext1_2 up
sudo ip link set veth_sf1_ext1_3 up
sudo ip link set veth_sf3_ext3_3 up
sudo ip link set veth_sf3_ext3_4 up

sudo ip netns exec server ip link set lo up
sudo ip netns exec client ip link set lo up
sudo ip netns exec sf2 ip link set lo up
sudo ip netns exec sf1 ip link set lo up
sudo ip netns exec sf3 ip link set lo up

sudo ip netns exec server ip route add default via 10.0.0.254 # the default gateway is the cl-eg
sudo ip netns exec client ip route add default via 10.0.0.253 # this address is for cl_in so I wonder if we should use the cl_eg for the server

# add routes inside the SFs
sudo ip netns exec sf1 ip route add 10.0.0.1/32 dev veth_sf1_int1_2
sudo ip netns exec sf1 ip route add 10.0.0.5/32 dev veth_sf1_int1_3
sudo ip netns exec sf2 ip route add 10.0.0.1/32 dev veth_sf2_int2_1
sudo ip netns exec sf2 ip route add 10.0.0.5/32 dev veth_sf2_int2_2
sudo ip netns exec sf3 ip route add 10.0.0.1/32 dev veth_sf3_int3_3
sudo ip netns exec sf3 ip route add 10.0.0.5/32 dev veth_sf3_int3_4

#enable forwarding for the SFs
sudo ip netns exec sf2 sysctl -w net.ipv4.ip_forward=1
sudo ip netns exec sf1 sysctl -w net.ipv4.ip_forward=1
sudo ip netns exec sf3 sysctl -w net.ipv4.ip_forward=1

#the interface ens3 i modified to eth0
sudo iptables -t nat -A POSTROUTING -s 10.0.0.0/24 -o eth0 -j MASQUERADE
sudo iptables -F
sudo iptables -P FORWARD ACCEPT

#Create routes between switches.
#link between cl_in - sff1
ip link add name link0_1 type veth peer name link1_0

ip link set link1_0 up

ip link set link0_1 up

ovs-vsctl add-port cl_in link0_1 -- set Interface link0_1 ofport_request=2

ovs-vsctl add-port sff1 link1_0 -- set Interface link1_0 ofport_request=2
#link between sff1-sff2
ip link add name link1_2 type veth peer name link2_1

ip link set link1_2 up

ip link set link2_1 up

ovs-vsctl add-port sff1 link1_2 -- set Interface link1_2 ofport_request=3

ovs-vsctl add-port sff2 link2_1 -- set Interface link2_1 ofport_request=2

#link between sff2 and sff3
ip link add name link2_3 type veth peer name link3_2

ip link set link2_3 up

ip link set link3_2 up

ovs-vsctl add-port sff2 link2_3 -- set Interface link2_3 ofport_request=3

ovs-vsctl add-port sff3 link3_2 -- set Interface link3_2 ofport_request=2
#link between sff3 and sff4
ip link add name link3_4 type veth peer name link4_3

ip link set link3_4 up

ip link set link4_3 up

ovs-vsctl add-port sff3 link3_4 -- set Interface link3_4 ofport_request=3

ovs-vsctl add-port sff4 link4_3 -- set Interface link4_3 ofport_request=2
#link between sff4 and sff5
ip link add name link4_5 type veth peer name link5_4

ip link set link4_5 up

ip link set link5_4 up

ovs-vsctl add-port sff4 link4_5 -- set Interface link4_5 ofport_request=3

ovs-vsctl add-port cl_eg link5_4 -- set Interface link5_4 ofport_request=2
echo "Topo created"

#sudo ip netns exec client ping -c 10 10.0.0.5
#sudo ip netns exec client tracepath 10.0.0.5
#CAP_WSRV_1=1000
#LAT_WSRV_1=10
#sudo tc qdisc add dev veth_client_ext root tbf rate ${CAP_WSRV_1}kbit burst 32kbit latency ${LAT_WSRV_1}ms

#sudo tc qdisc add dev veth_client_ext root tbf rate $1kbit burst 32kbit latency $2ms

