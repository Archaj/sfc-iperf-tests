from matplotlib import pyplot as plt
import numpy as np
from numpy import genfromtxt
import pandas as pd
def prepare_data (file_name):
    #my_data= genfromtxt('bandwidth_all.csv')
    my_data = np.genfromtxt(file_name, delimiter = ',')
    #n is the number of rows, where each line is a 1 Second time interval
    n=np.shape(my_data)[0]
    index = np.arange(1, n+1, 1)
    mean = []
    ci95= []
    for y in my_data:
        m= np.mean(y)
        ci = 1.96 * (np.std(y)/m)
        mean.append(m)
        ci95.append(ci)

    return (index,mean,ci95)

def plotting(x, y1_avg, y1_ci,y_title,fig_title):
        fig, ax = plt.subplots()
        # par1 = ax.twinx()
        ax.grid(color='black', linestyle='-', linewidth=0.1)

        p1 = ax.errorbar(x, y1_avg, color='blue', yerr=y1_ci, label="Time", fmt='s--')
        #p1= ax.fill_between(x,(y1_avg - y1_ci), (y1_avg + y1_ci) , color='g' , alpha=0.1 )
        ax.set_xlim()
        ax.set_ylabel(y_title, fontsize=16)
        ax.set_xlabel("Time (sec)", fontsize=16)
        tkw = dict(size=4, width=0.1)
        ax.tick_params(labelsize=15, axis='y', **tkw)
        ax.tick_params(labelsize=15, axis='x', **tkw)
        lines = [p1]
        art = []
        plt.savefig( fig_title,additional_artists=art, bbox_inches="tight", dpi=1000)


    #fig, ax = plt.subplots()
    #ax.plot(x,y)
   # ax.fill_between(x, (y-ci),(y+ci), color='b', alpha=0.1)
# plotting full symmetry results
(indexC1, expMeanC1, ci95C1) = prepare_data('results/full_symmetry/bandwidth_all.csv')
plotting(indexC1, expMeanC1, ci95C1, "Throughput (Mbits/sec)",'results/full_symmetry/throughput_full.eps')

(indexC1, expMeanC1, ci95C1) = prepare_data('results/full_symmetry/transfer_all.csv')
plotting(indexC1, expMeanC1, ci95C1, "Transfer (MBytes)",'results/full_symmetry/transfer_full.eps')

(indexC1, expMeanC1, ci95C1) = prepare_data('results/full_symmetry/rtt_all.csv')
plotting(indexC1, expMeanC1, ci95C1, "RTT (sec)",'results/full_symmetry/rtt_full.eps')

# plotting partial symmetry results
(indexC1, expMeanC1, ci95C1) = prepare_data('results/partial_symmetry/bandwidth_all.csv')
plotting(indexC1, expMeanC1, ci95C1, "Throughput (Mbits/sec)",'results/partial_symmetry/throughput_partial.eps')

(indexC1, expMeanC1, ci95C1) = prepare_data('results/partial_symmetry/transfer_all.csv')
plotting(indexC1, expMeanC1, ci95C1, "Transfer (MBytes)",'results/partial_symmetry/transfer_partial.eps')

(indexC1, expMeanC1, ci95C1) = prepare_data('results/partial_symmetry/rtt_all.csv')
plotting(indexC1, expMeanC1, ci95C1, "RTT (sec)",'results/partial_symmetry/rtt_partial.eps')
