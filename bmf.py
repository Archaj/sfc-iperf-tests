# This code would be used to generate the Fractional Brownian Motion traffic

# flake8: noqa
from fbm import MBM
from fbm import FBM
import matplotlib.pyplot as plt
import time
import math


def h(t):
    # return 0.499*math.sin(t) + 0.5
    # return 0.6 * t + 0.3
    return 0.5 * math.exp(-8 * t ** 2) + 0.35



m = FBM(n=1024, hurst=0.75, length=1, method='daviesharte')
t = m.times()
mbm = m.fgn()

plt.plot(t, mbm)
plt.plot(t, [h(tt) for tt in t])
plt.show()

