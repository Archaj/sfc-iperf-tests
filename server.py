import shlex
from subprocess import Popen, PIPE
from sys import stderr
from subprocess import Popen, PIPE
from sys import stderr
import time
server_args= shlex.split("ip netns exec server iperf3 -s -p 5565 -i 1 --logfile results/server.txt")

print(server_args)
iperf_server_process = Popen(server_args, stdout=PIPE, stderr=stderr)
retcode=iperf_server_process.poll()

# I need to wait to keep the server up
time.sleep(100)
#iperf_server_process.terminate()
#iperf_server_process.kill()
print("Return code:")
print(iperf_server_process.returncode)
iperf_server_process.terminate()
print(iperf_server_process.returncode)
iperf_server_process.kill()