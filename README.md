The topology used in this project is :
![](images/draft_figure_topology.jpg)

The following list describes the interfaces attatched to each host/sf, including the switch port it is attached to, switch, and role of interface(output/input) and the ip address.
![](images/hosts_interfaces_description.png)

